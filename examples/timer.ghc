% Show a simple timer using curses.

-initialization(main).

-mode(state(^)).
state(State) :- State := state(0, 0).

main :-
    i18n:lc_all(LcAll),
    i18n:setlocale(LcAll, '', _) &
    curses:prologue(_) &
    curses:curs_set(0, _) &
    (curses:get_stdscr(StdScr),
     curses:curs_true(CursTrue),
     curses:nodelay(StdScr, CursTrue, _),
     curses:getch(Ch),
     state(State0),
     main_loop(Ch, State0, _)).

-mode(main_loop(?, ?, ^)).
main_loop(Ch)-_ :- Ch =:= 0'q | curses:endwin(_).
main_loop(Ch)-_ :- Ch =:= 0'Q | curses:endwin(_).
main_loop(Ch)-State :- otherwise |
    curses:clear(_) &
    paint_time(State) &
    curses:refresh(_) &
    check_for_space(Ch)-State.

-mode(check_for_space(?, ?, ^)).
check_for_space(Ch0)-State :- Ch0 =:= 32 |
    start_stop_clear-State,
    curses:getch(Ch),
    main_loop(Ch)-State.
check_for_space(_)-State :- otherwise |
    curses:getch(Ch),
    main_loop(Ch)-State.

% Start, stop, and clear the timer successively.
-mode(start_stop_clear(?, ^)).
start_stop_clear(State0, State) :-
    State0 = state(Start, Stop),
    start_stop_clear2(Start, Stop, State).

-mode(start_stop_clear2(?, ?, ^)).
start_stop_clear2(_, Stop, State) :- Stop \=:= 0 | State := state(0, 0).
start_stop_clear2(Start, _, State) :- Start =:= 0 |
    posix:clock_realtime(Clock),
    posix:mktimespec(NowStruct),
    posix:clock_gettime(Clock, NowStruct, Done),
    when(Done, posix:timespec_tv_sec(NowStruct, Now)),
    State := state(Now, 0).
start_stop_clear2(Start, _, State) :- otherwise |
    posix:clock_realtime(Clock),
    posix:mktimespec(NowStruct),
    posix:clock_gettime(Clock, NowStruct, Done),
    when(Done, posix:timespec_tv_sec(NowStruct, Now)),
    State := state(Start, Now).

% Compute the time elapsed since START (to END if set).
% If the timer hasn't started, return NIL.
-mode(time_elapsed(?, ^)).
time_elapsed(state(Start, Stop), Dt) :- Start \=:= 0, Stop =:= 0 |
    posix:clock_realtime(Clock),
    posix:mktimespec(NowStruct),
    posix:clock_gettime(Clock, NowStruct, Done),
    when(Done, posix:timespec_tv_sec(NowStruct, Now)),
    Dt is Now - Start.
time_elapsed(state(Start, Stop), Dt) :- Start \=:= 0, Stop \=:= 0 | Dt is Stop - Start.
time_elapsed(state(Start, _), Dt) :- Start =:= 0 | Dt := [].

% Paint the elapsed time to the center of the screen.
-mode(paint_time(?)).
paint_time(State) :-
    curses:get_stdscr(StdScr),
    curses:getmaxyx(StdScr, Height, Width),
    time_elapsed(State, Dt),
    format_time(Dt, DtCharList),
    list_to_string(DtCharList, DtStr),
    length(DtStr, TimeLen),
    HalfLength is floor(TimeLen / 2),
    X is integer(floor(Width / 2) - HalfLength),
    Y is integer(Height / 2),
    curses:move(Y, X, Done),
    when(Done, curses:addstr(DtStr, _)).

-mode(format_time(?, ^)).
format_time(T, TStr) :- T == [] | TStr = 'Press [SPACE] to start/stop/clear'.
format_time(T, TStr) :- T =\= [] | fmt:format_chars("~d", [T], TStr).
