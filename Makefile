# makefile for flenglib

.POSIX:

OPSYS := $(shell uname)
ifeq ($(OPSYS),Darwin)
NCURSES_CFLAGS := $(shell ncurses6-config --cflags)
NCURSES_FLAGS := $(NCURSES_CFLAGS) $(shell ncurses6-config --libs)
else
NCURSES_FLAGS := -lncurses
endif

.PHONY: all clean install examples documentation

all:
	env NCURSES_CFLAGS="$(NCURSES_CFLAGS)" flengmake -r flenglib

clean:
	flengmake -r flenglib clean
	flengmake -r examples clean

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/lib $(DESTDIR)$(PREFIX)/bin
	cp flenglib/libflenglib.a $(DESTDIR)$(PREFIX)/lib
	install flenglibs $(DESTDIR)$(PREFIX)/bin

examples: all
	env FLENGLIB_PREFIX=$$(pwd) NCURSES_FLAGS="$(NCURSES_FLAGS)" flengmake -r examples

documentation:
	flengdoc -c flenglib/*.ghc
