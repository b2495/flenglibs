%= NDBM interface
%
% A limitation of this implementation is that only printable string keys
% and values have been tested.
%
%-
% ndbm:open(FILENAME?, MODE?, DB^)
%       Opens the database in file FILENAME. MODE is the UNIX file mode for a created file.
%       DB is unified with a port accepting the following messages.
%       Uses the 'dbm_open' library function.
%_
%    Valid request messages are the following:
%_
%       close
%        Close the database file. This is strictly unnecessary, as it
%        will be automatically done when the port goes out of scope.
%_
%       store(KEY?, VAL?, DONE^)
%        Store a tuple in the database with key KEY, value VAL.
%        A 'store_mode' of 'DBM_REPLACE' is always used to resolve key conflicts.
%        After the data is written, DONE is unified with the empty list.
%        Corresponds to the 'dbm_store' library function.
%_
%       append(KEY?, VAL?, DONE^)
%        Append VAL to the current value associated with KEY in the database.
%        After the data is written, DONE is unified with the empty list.
%_
%       fetch(KEY?, VAL^, TAIL?)
%        Fetch the value for key KEY from the database, then append TAIL to it.
%        Uses the 'dbm_fetch' library function.
%_
%       write(KEY?, FILE?, FOUND^)
%        Write the value associated with KEY (if any) to an open file FILE.
%        FOUND will be set to the string "true" or "false" depending on
%        whether such a value existed, or not, respectively.
%_
%       first(KEY^)
%        Set KEY to the first key in the database.
%_
%       next(KEY?, NEXTKEY^)
%        Get the next key after KEY in the database.
%        After the end of database is reached, NEXTKEY is set to the empty list.

-module(ndbm).

open(Name, Mode, DB) :-
    open_port(DB, S),
    db_open(Name, Mode, DBref),
    open2(DBref, Name, Mode, S).

db_open(Name, !Mode, DBref) :-
    deref(Name, Ok),
    when(Ok, foreign_call(fl_ndbm_open(Name, Mode, DBref))).

open2(false, Name, Mode, _) :- error('ndbm: can not open database'(Name, Mode)).
open2(DBref, _, _, S) :- otherwise | loop(S, DBref).

loop([], DBref) :- foreign_call(fl_ndbm_close(DBref, _)).
loop([close|_], DBref) :- loop([], DBref).
loop([store(Key, Val, Done)|S], DBref) :-
    deref(Key/Val, Ok),
    when(Ok, foreign_call(fl_ndbm_store(DBref, Key, Val, Done))),
    when(Done, loop(S, DBref)).
loop([append(Key, Val, Done)|S], DBref) :-
    deref(Key/Val, Ok), 
    when(Ok, foreign_call(fl_ndbm_append(DBref, Key, Val, Done))),
    when(Done, loop(S, DBref)).
loop([fetch(Key, Val, Tail)|S], DBref) :-
    deref(Key, Ok),
    when(Ok, foreign_call(fl_ndbm_fetch(DBref, Key, Val, Tail))),
    db_fetch(Val, Key, S, DBref).
loop([write(Key, !File, Found)|S], DBref) :-
    deref(Key, Ok),
    when(Ok, foreign_call(fl_ndbm_write(DBref, Key, File, Found))),
    db_fetch(Found, Key, S, DBref).
loop([first(Key)|S], DBref) :-
    foreign_call(fl_ndbm_first_key(DBref, Key)),
    when(Key, loop(S, DBref)).
loop([next(Key1, Key)|S], DBref) :-
    deref(Key1, Ok),
    when(Ok, foreign_call(fl_ndbm_next_key(DBref, Key1, Key))),
    when(Key, loop(S, DBref)).

db_fetch(false, Key, _, _) :- error('ndbm: key not found'(Key)).
db_fetch(_, _, S, DBref) :- otherwise | loop(S, DBref).
