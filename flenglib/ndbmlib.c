/* interface to NDBM database */

#include <fleng.h>
#include <fleng-util.h>
#include <ndbm.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

void fl_ndbm_open_3(FL_TCB *tcb, FL_VAL name, FL_VAL mode, FL_VAL var)
{
    CHECK_STRING(mode);
    int len;
    char *p = stringify(tcb, name, &len);
    int flags = O_RDONLY;
    for(char *m = STRING(mode); *m != '\0'; ++m) {
        switch(*m) {
        case 'r': flags = O_RDONLY; break;
        case 'w': flags = O_RDWR | O_CREAT; break;
        case 'c': flags = O_RDWR | O_CREAT; break;
        }
    }
    DBM *db = dbm_open(p, flags, 0766);
    if(db == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, MARK(db), var);
}

void fl_ndbm_close_2(FL_TCB *tcb, FL_VAL db, FL_VAL done)
{
    CHECK_INT(db);
    DBM *p = (DBM *)UNMARK(db);
    dbm_close(p);
    fl_assign(tcb, fl_nil, done);
}

void fl_ndbm_store_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL val, FL_VAL done)
{
    CHECK_INT(db);
    DBM *p = (DBM *)UNMARK(db);
    datum k, v;
    int temp_dsize;
    char *buf = stringify(tcb, key, &temp_dsize);
    k.dsize = temp_dsize;
    k.dptr = strndup(buf, k.dsize);
    v.dptr = stringify(tcb, val, &temp_dsize);
    v.dsize = temp_dsize;
    dbm_store(p, k, v, DBM_REPLACE);
    free(k.dptr);
    fl_assign(tcb, fl_nil, done);
}

void fl_ndbm_append_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL val, FL_VAL done)
{
    CHECK_INT(db);
    DBM *p = (DBM *)UNMARK(db);
    datum k, v;
    int temp_dsize;
    char *buf = stringify(tcb, key, &temp_dsize);
    k.dsize = temp_dsize;
    k.dptr = strndup(buf, k.dsize);
    v = dbm_fetch(p, k);
    int f = 0;
    if(v.dptr == NULL) {
        v.dptr = stringify(tcb, val, &temp_dsize);
        v.dsize = temp_dsize;
    } else {
        int len;
        char *vp = stringify(tcb, val, &len);
        char *tmp = malloc(v.dsize + len);
        memcpy(tmp, v.dptr, v.dsize);
        memcpy(tmp + v.dsize, vp, len);
        v.dsize += len;
        v.dptr = tmp;
        f = 1;
    }
    dbm_store(p, k, v, DBM_REPLACE);
    free(k.dptr);
    if(f) free(v.dptr);
    fl_assign(tcb, fl_nil, done);
}

void fl_ndbm_fetch_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL result, FL_VAL tail)
{
    CHECK_INT(db);
    DBM *p = (DBM *)UNMARK(db);
    datum k, v;
    int k_dsize;
    k.dptr = stringify(tcb, key, &k_dsize);
    k.dsize = k_dsize;
    v = dbm_fetch(p, k);
    if(v.dptr == NULL) fl_assign(tcb, fl_false, result);
    else {
        FL_VAL lst = mkcharlist(tcb, v.dptr, v.dsize, tail);
        fl_assign(tcb, lst, result);
    }
}

void fl_ndbm_write_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL file, FL_VAL var)
{
    CHECK_INT(db);
    CHECK_INT(file);
    DBM *p = (DBM *)UNMARK(db);
    datum k, v;
    int k_dsize;
    k.dptr = stringify(tcb, key, &k_dsize);
    k.dsize = k_dsize;
    v = dbm_fetch(p, k);
    if(v.dptr == NULL) fl_assign(tcb, fl_false, var);
    else {
        if(write(INT(file), v.dptr, v.dsize) < 0)
            fl_rt_error(tcb, key, FL_IO_ERROR);
        fl_assign(tcb, fl_true, var);
    }
}

void fl_ndbm_first_key_2(FL_TCB *tcb, FL_VAL db, FL_VAL key)
{
    CHECK_INT(db);
    DBM *p = (DBM *)UNMARK(db);
    datum k = dbm_firstkey(p);
    if(k.dptr == NULL) fl_assign(tcb, fl_nil, key);
    else {
        FL_VAL lst = mkcharlist(tcb, k.dptr, k.dsize, fl_nil);
        fl_assign(tcb, lst, key);
    }
}

void fl_ndbm_next_key_3(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL rkey)
{
    CHECK_INT(db);
    DBM *p = (DBM *)UNMARK(db);
    datum k;
    int k_dsize;
    k.dptr = stringify(tcb, key, &k_dsize);
    k.dsize = k_dsize;
    k = dbm_nextkey(p);
    if(k.dptr == NULL) fl_assign(tcb, fl_nil, rkey);
    else {
        FL_VAL lst = mkcharlist(tcb, k.dptr, k.dsize, fl_nil);
        fl_assign(tcb, lst, rkey);
    }
}

