/* interface to GDBM database */

#include <fleng.h>
#include <fleng-util.h>
#include <gdbm.h>
#include <string.h>
#include <unistd.h>

static void fatal(const char *msg) 
{
    fprintf(stderr, "\nFatal GDBM error: %s\n", msg);
    fl_terminate(3);
}

void fl_gdbm_open_3(FL_TCB *tcb, FL_VAL name, FL_VAL mode, FL_VAL var)
{
    CHECK_STRING(mode);
    int len;
    char *p = stringify(tcb, name, &len);
    int flags = GDBM_READER;
    for(char *m = STRING(mode); *m != '\0'; ++m) {
        switch(*m) {
        case 'r': flags = GDBM_READER; break;
        case 'w': flags = GDBM_WRCREAT; break;
        case 'c': flags = GDBM_NEWDB; break;
        }
    }
    GDBM_FILE db = gdbm_open(p, 0, flags, 0766, fatal);
    if(db == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, MARK(db), var);
}

void fl_gdbm_close_2(FL_TCB *tcb, FL_VAL db, FL_VAL done)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    gdbm_close(p);
    fl_assign(tcb, fl_nil, done);
}

void fl_gdbm_store_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL val, FL_VAL done)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k, v;
    char *buf = stringify(tcb, key, &k.dsize);
    k.dptr = strndup(buf, k.dsize);
    v.dptr = stringify(tcb, val, &v.dsize);
    gdbm_store(p, k, v, GDBM_REPLACE);
    free(k.dptr);
    fl_assign(tcb, fl_nil, done);
}

void fl_gdbm_append_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL val, FL_VAL done)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k, v;
    char *buf = stringify(tcb, key, &k.dsize);
    k.dptr = strndup(buf, k.dsize);
    v = gdbm_fetch(p, k);
    int f = 0;
    if(v.dptr == NULL)
        v.dptr = stringify(tcb, val, &v.dsize);
    else {
        int len;
        char *vp = stringify(tcb, val, &len);
        char *tmp = malloc(v.dsize + len);
        memcpy(tmp, v.dptr, v.dsize);
        memcpy(tmp + v.dsize, vp, len);
        v.dsize += len;
        v.dptr = tmp;
        f = 1;
    }
    gdbm_store(p, k, v, GDBM_REPLACE);
    free(k.dptr);
    if(f) free(v.dptr);
    fl_assign(tcb, fl_nil, done);
}

void fl_gdbm_exists_3(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL result)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k, v;
    k.dptr = stringify(tcb, key, &k.dsize);
    fl_assign(tcb, gdbm_exists(p, k) ? fl_true : fl_false, result);
}

void fl_gdbm_fetch_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL result, FL_VAL tail)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k, v;
    k.dptr = stringify(tcb, key, &k.dsize);
    v = gdbm_fetch(p, k);
    if(v.dptr == NULL) fl_assign(tcb, fl_false, result);
    else {
        FL_VAL lst = mkcharlist(tcb, v.dptr, v.dsize, tail);
        free(v.dptr);
        fl_assign(tcb, lst, result);
    }
}

void fl_gdbm_write_4(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL file, FL_VAL var)
{
    CHECK_INT(db);
    CHECK_INT(file);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k, v;
    k.dptr = stringify(tcb, key, &k.dsize);
    v = gdbm_fetch(p, k);
    if(v.dptr == NULL) fl_assign(tcb, fl_false, var);
    else {
        if(write(INT(file), v.dptr, v.dsize) < 0) 
            fl_rt_error(tcb, key, FL_IO_ERROR);
        fl_assign(tcb, fl_true, var);
    }
}

void fl_gdbm_count_2(FL_TCB *tcb, FL_VAL db, FL_VAL num)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    gdbm_count_t c = 0;
    gdbm_count(p, &c);
    fl_assign(tcb, MKINT(c), num);
}

void fl_gdbm_first_key_2(FL_TCB *tcb, FL_VAL db, FL_VAL key)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k = gdbm_firstkey(p);
    if(k.dptr == NULL) fl_assign(tcb, fl_nil, key);
    else {
        FL_VAL lst = mkcharlist(tcb, k.dptr, k.dsize, fl_nil);
        free(k.dptr);
        fl_assign(tcb, lst, key);
    }
}

void fl_gdbm_next_key_3(FL_TCB *tcb, FL_VAL db, FL_VAL key, FL_VAL rkey)
{
    CHECK_INT(db);
    GDBM_FILE p = (GDBM_FILE)UNMARK(db);
    datum k;
    k.dptr = stringify(tcb, key, &k.dsize);
    k = gdbm_nextkey(p, k);
    if(k.dptr == NULL) fl_assign(tcb, fl_nil, rkey);
    else {
        FL_VAL lst = mkcharlist(tcb, k.dptr, k.dsize, fl_nil);
        free(k.dptr);
        fl_assign(tcb, lst, rkey);
    }
}

